# FSW5 Challenge Chapter05

## Car Management Dashboard

Pada Challenge Chapter05 kali ini kami diberikan tugas untuk membuat sebuah dashboard yang mengelola data mobil dengan menerapkan metode CRUD menggunakan view engine, membuat Rest API, sekaligus terhubung ke database yang digunakan.

### ER Diagram Pada Project Ini:

![Diagram_Challenge05__1_](/uploads/8dbc3cfd631cf4127c49026ac1a63a3f/Diagram_Challenge05__1_.png)

### Daftar Link Untuk Tiap Halaman:

1. Halaman Utama : http://localhost:8080/
2. Laman Add New Car : http://localhost:8080/add
3. Laman Update Car Information : http://localhost:8080/edit/id

Disini kami menggunakan view engine EJS untuk menampilkan tiap halaman.

### Endpoint Rest API pada Project ini:

1. Get All Data : http://localhost:8080/api/  => router.get('/', async (req, res) => { })
2. Get Data by Id : http://localhost:8080/api/ => router.get('/:id', getApi, (req, res) => { })
3. Post Data : http://localhost:8080/api/ => router.post('/', async (req, res) => { })
4. Update Data by Id : http://localhost:8080/api/id => router.put('/:id', async (req, res) => { })
5. Delete Data by Id : http://localhost:8080/api/id => router.delete('/:id', getApi, async (req, res) => { })

Database yang digunakan pada project ini adalah MongoDB
