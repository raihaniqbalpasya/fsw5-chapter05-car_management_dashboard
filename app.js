require('dotenv').config()
const express = require('express')
const mongoose = require('mongoose')
const session = require('express-session')
const filter = require("./service/service")

const app = express()
const PORT = process.env.PORT || 3004

// database connection
mongoose.connect(process.env.DB_URI, { useNewUrlParser: true, useUnifiedTopology : true })
const db = mongoose.connection
db.on('error', (error) => console.log(error))
db.once('open', () => console.log('Connected to Database'))

// middlewares
app.use(express.urlencoded({extended:false}))
app.use(express.json());
app.use(session ({
    secret: 'my secret key',
    saveUninitialized: true,
    resave: false,
}))
app.use((req, res, next) => {
    res.locals.message = req.session.message
    delete req.session.message
    next()
})

// call api route
const apiRouter = require('./routes/api')
app.use('/api', apiRouter)

// get filter function
app.get("/filter/:size", filter.filter);

// load design and image
app.use(express.static('public/image'))
app.use(express.static('public'));

// set ejs's view
app.set('view engine', 'ejs');

// call view routes
app.use("", require('./routes/routes'))

// run the server
app.listen(PORT, () => {
    console.log(`Example app listening on port http://localhost:${PORT}`)
})