const mongoose = require('mongoose')
const carSchema = new mongoose.Schema({
    nama: {
        type: String,
        required:true
    },
    tipe: {
        type: String,
        required:true
    },
    harga: {
        type: Number,
        required:true
    },
    ukuran: {
        type: String,
        required: true
    },
    image: {
        type: String,
        required: true
    },
    created: {
        type: Date,
        required:true,
        default: Date.now
    }
})

module.exports = mongoose.model("Car", carSchema)