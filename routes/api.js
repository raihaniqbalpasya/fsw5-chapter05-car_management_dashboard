const express = require('express')
const router = express.Router()
const apiModels = require('../models/cars')

// get all
router.get('/', async (req, res) => {
    try {
        const apis = await apiModels.find()
        res.json(apis)
    } catch (error) {
        res.status(500).json({message: err.message})
    }
})

// get by id
router.get('/:id', getApi, (req, res) => {
    res.send(res.api)
})

// post
router.post('/', async (req, res) => {
    const api = new apiModels({
        nama: req.body.nama,
        tipe: req.body.tipe,
        harga: req.body.harga,
        ukuran: req.body.ukuran,
        image: req.body.image
    })
    try {
        const newApi = await api.save()
        res.status(201).json(newApi)
    } catch (error) {
        res.status(400).json({message: err.message})
    }
})

// update by id
router.put('/:id', async (req, res) => {
    try {
        const updateApi = await apiModels.findByIdAndUpdate(req.params.id, {$set: req.body}, {new:true})
        res.json(updateApi)
    } catch (error) {
        res.status(400).json({message: err.message})
    }
})

// delete by id
router.delete('/:id', getApi, async (req, res) => {
    try {
        await res.api.remove()
        res.json({message:'Data mobil berhasil dihapus'})
    } catch (error) {
        res.status(500).json({message: err.message})
    }
})

async function getApi(req, res, next){
    let api
    try {
        api = await apiModels.findById(req.params.id)
        if(api == null){
            return res.status(404).json({message: 'Id tidak ditemukan'})
        }
    } catch (error) {
        return res.status(500).json({message:err.message})
    }
    res.api = api
    next()
}

module.exports = router