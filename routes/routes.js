const express = require('express')
const router = express.Router()
const Car = require('../models/cars')
const multer = require('multer')
const fs = require('fs')

// image upload
var storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './public/image/')
    },
    filename: function(req, file, cb) {
        cb(null, file.fieldname + "_" + Date.now() + "_" + file.originalname)
    }
})
var upload = multer({
    storage: storage,
}).single("image")

// input data car
router.post('/add', upload, (req, res) => {
    const car = new Car({
        nama: req.body.nama,
        tipe: req.body.tipe,
        harga: req.body.harga,
        ukuran: req.body.ukuran,
        image: req.file.filename
    })
    car.save((err) => {
        if(err){
            res.json({message: err.message, type: 'success'})
        } else {
            req.session.message = {
                type: 'success',
                message: 'Data berhasil disimpan'
            }
            res.redirect('/')
        }
    })
})

// get all cars route
router.get('/', (req, res) => {
    Car.find().exec((err, cars) => {
        if(err){
            res.json({message: err.message})
        } else {
            res.render('index', {
                title: "Car Management Dashboard",
                cars: cars,
            })
        }
    })
})

router.get('/add', (req, res) => {
    res.render('add', {title: 'Add Data'})
})

// edit car route
router.get('/edit/:id', (req, res) => {
    let id = req.params.id
    Car.findById(id, (err, car) => {
        if(err){
            res.redirect('/')
        } else {
            if(car == null){
                res.redirect('/')
            } else {
                res.render('edit', {
                    title: "Edit Data",
                    car: car,
                })
            }
        }
    })
})

// update cars route
router.post('/update/:id', upload, (req, res) => {
    let id = req.params.id
    let new_image = ''

    if(req.file){
        new_image = req.file.filename
        try{
            fs.unlinkSync('public/image/' + req.body.old_image)
        } catch(err){
            console.log(err)
        }
    } else {
        new_image = req.body.old_image
    }

    Car.findByIdAndUpdate(id, {
        nama: req.body.nama,
        tipe: req.body.tipe,
        harga: req.body.harga,
        ukuran: req.body.ukuran,
        image: new_image,
    }, (err, result) => {
        if(err){
            res.json({message: err.message, type: 'success'})
        } else {
            req.session.message = {
                type: 'success',
                message: 'Data berhasil diubah'
            }

            res.redirect('/')
        }
    })
})

// delete cars route
router.get('/delete/:id', (req, res) => {
    let id = req.params.id
    Car.findByIdAndRemove(id, (err, result) => {
        if(result.image != ''){
            try {
                fs.unlinkSync('./public/image/' + result.image)
            } catch (err) {
                console.log(err)
            }
        }

        if(err){
            res.json({message: err.message})
        } else {
            req.session.message = {
                type: "info",
                message: "Data berhasil dihapus"
            }
            res.redirect('/')
        }
    })
})

module.exports = router

// app.get('/', (req, res) => {
//     res.render('index', {
//         title: "List Cars"
//     })
// });

// app.get('/edit', (req, res) => {
//     res.render('edit', {
//         title: "Update Car Information"
//     })
// });

// app.get('/add', (req, res) => {
//     res.render('add', {
//         title: "Add New Car"
//     })
// });

// app.get('/index2', (req, res) => {
//     res.render('index2', {})
// });

// app.use('/', (req, res) => {
//     res.status(404).render('404');
// });

