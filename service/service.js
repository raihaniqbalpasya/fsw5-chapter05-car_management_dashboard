const Car  = require("../models/cars");

const filter = async (req, res) => {
    const carList = await Car.find();
    let newcar = JSON.stringify(carList, null, 2);
    newcar = JSON.parse(newcar);

    const car = newcar.filter((row) => row.ukuran == req.params.ukuran);

    res.render("index", {
        title: 'Filter Data',
        cars: car,
    });
};

module.exports = {filter}